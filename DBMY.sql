USE [master]
GO
/****** Object:  Database [QLTTHCC]    Script Date: 10/23/2019 11:58:16 AM ******/
CREATE DATABASE [QLTTHCC]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QLTTHCC', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\QLTTHCC.mdf' , SIZE = 3136KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'QLTTHCC_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\QLTTHCC_log.ldf' , SIZE = 784KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [QLTTHCC] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QLTTHCC].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QLTTHCC] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QLTTHCC] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QLTTHCC] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QLTTHCC] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QLTTHCC] SET ARITHABORT OFF 
GO
ALTER DATABASE [QLTTHCC] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [QLTTHCC] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [QLTTHCC] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QLTTHCC] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QLTTHCC] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QLTTHCC] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QLTTHCC] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QLTTHCC] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QLTTHCC] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QLTTHCC] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QLTTHCC] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QLTTHCC] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QLTTHCC] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QLTTHCC] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QLTTHCC] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QLTTHCC] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QLTTHCC] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QLTTHCC] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QLTTHCC] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [QLTTHCC] SET  MULTI_USER 
GO
ALTER DATABASE [QLTTHCC] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QLTTHCC] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QLTTHCC] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QLTTHCC] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [QLTTHCC]
GO
/****** Object:  Table [dbo].[DanhGia]    Script Date: 10/23/2019 11:58:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DanhGia](
	[MaDG] [int] IDENTITY(1,1) NOT NULL,
	[MaCB] [int] NULL,
	[MaNDG] [int] NULL,
	[MaBanN] [int] NULL,
	[NoiDung] [nvarchar](max) NULL,
	[MaBienNhan] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaDG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DoHaiLong]    Script Date: 10/23/2019 11:58:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DoHaiLong](
	[MaDHL] [int] IDENTITY(1,1) NOT NULL,
	[MaDanhGia] [int] NULL,
	[MaBienNhan] [int] NULL,
	[SoPhanTram] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaDHL] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HoSo]    Script Date: 10/23/2019 11:58:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HoSo](
	[MaCB] [int] IDENTITY(1,1) NOT NULL,
	[TenCB] [nvarchar](50) NULL,
	[NgaySinh] [varchar](11) NULL,
	[ChucVu] [nvarchar](100) NULL,
	[MaSo] [int] NULL,
	[DiaChi] [nvarchar](200) NULL,
	[SDT] [int] NULL,
	[NgayDauNhanViec] [varchar](11) NULL,
	[MaDHL] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaCB] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NguoiDanhGia]    Script Date: 10/23/2019 11:58:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NguoiDanhGia](
	[MaNDG] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[NgaySinh] [varchar](10) NULL,
	[DT] [int] NULL,
	[MaBienNhan] [int] NULL,
	[ThuongTru] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[MaNDG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NguoiDung]    Script Date: 10/23/2019 11:58:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NguoiDung](
	[MaND] [int] IDENTITY(1,1) NOT NULL,
	[MaCB] [int] NULL,
	[TenDangNhap] [varchar](50) NULL,
	[MatKhau] [varchar](50) NULL,
	[PhanQuyen] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaND] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NguoiPhanAnh]    Script Date: 10/23/2019 11:58:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NguoiPhanAnh](
	[MaNPA] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[NgaySinh] [varchar](10) NULL,
	[DT] [int] NULL,
	[MaBienNhan] [int] NULL,
	[ThuongTru] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[MaNPA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NoiDung]    Script Date: 10/23/2019 11:58:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NoiDung](
	[MaNoiD] [int] IDENTITY(1,1) NOT NULL,
	[NoiDung] [nvarchar](max) NULL,
	[NoiDungChiTiet] [nvarchar](max) NULL,
	[MaND] [int] NULL,
	[MaDG] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaNoiD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PhanAnh]    Script Date: 10/23/2019 11:58:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhanAnh](
	[MaPA] [int] IDENTITY(1,1) NOT NULL,
	[MaNPA] [int] NULL,
	[HinhAnh] [nvarchar](300) NULL,
	[NoiDung] [nvarchar](max) NULL,
	[MaBienNhan] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MaPA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SoNganh]    Script Date: 10/23/2019 11:58:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SoNganh](
	[MaS] [int] IDENTITY(1,1) NOT NULL,
	[TenS] [nvarchar](100) NULL,
	[SoLuong] [int] NULL,
	[Hinh] [nvarchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[MaS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[DanhGia] ON 

INSERT [dbo].[DanhGia] ([MaDG], [MaCB], [MaNDG], [MaBanN], [NoiDung], [MaBienNhan]) VALUES (1, 1, 1, 1, N'sdfghj', 1233345667)
INSERT [dbo].[DanhGia] ([MaDG], [MaCB], [MaNDG], [MaBanN], [NoiDung], [MaBienNhan]) VALUES (10, 1, 29, 1, N'Kiểm Tra Thử', 12356)
INSERT [dbo].[DanhGia] ([MaDG], [MaCB], [MaNDG], [MaBanN], [NoiDung], [MaBienNhan]) VALUES (11, 1, 30, 1, N'abcd', 123457891)
INSERT [dbo].[DanhGia] ([MaDG], [MaCB], [MaNDG], [MaBanN], [NoiDung], [MaBienNhan]) VALUES (12, 1, 32, 1, NULL, 4531)
INSERT [dbo].[DanhGia] ([MaDG], [MaCB], [MaNDG], [MaBanN], [NoiDung], [MaBienNhan]) VALUES (13, 1, 33, 1, N'HahAhaha', NULL)
SET IDENTITY_INSERT [dbo].[DanhGia] OFF
SET IDENTITY_INSERT [dbo].[DoHaiLong] ON 

INSERT [dbo].[DoHaiLong] ([MaDHL], [MaDanhGia], [MaBienNhan], [SoPhanTram]) VALUES (1, 1, 1234567890, 80)
INSERT [dbo].[DoHaiLong] ([MaDHL], [MaDanhGia], [MaBienNhan], [SoPhanTram]) VALUES (2, 2, 456124577, 98)
INSERT [dbo].[DoHaiLong] ([MaDHL], [MaDanhGia], [MaBienNhan], [SoPhanTram]) VALUES (3, 3, 428760399, 78)
SET IDENTITY_INSERT [dbo].[DoHaiLong] OFF
SET IDENTITY_INSERT [dbo].[HoSo] ON 

INSERT [dbo].[HoSo] ([MaCB], [TenCB], [NgaySinh], [ChucVu], [MaSo], [DiaChi], [SDT], [NgayDauNhanViec], [MaDHL]) VALUES (1, N'Nguyen Van A', N'1/1/1994', N'CBCC', 1, N'TDM', 123456789, N'1/1/2013', NULL)
INSERT [dbo].[HoSo] ([MaCB], [TenCB], [NgaySinh], [ChucVu], [MaSo], [DiaChi], [SDT], [NgayDauNhanViec], [MaDHL]) VALUES (2, N'Nguyễn Thị B', N'31/5/1969', N'CBCC', 5, N'TDM,BD', 123456789, N'13/2/1996', 2)
INSERT [dbo].[HoSo] ([MaCB], [TenCB], [NgaySinh], [ChucVu], [MaSo], [DiaChi], [SDT], [NgayDauNhanViec], [MaDHL]) VALUES (3, N'Le Thi H Q', N'03/08/1972', N'CBCC', 10, N'PH,TDM,BD', 789453123, N'14/12/1995', 1)
SET IDENTITY_INSERT [dbo].[HoSo] OFF
SET IDENTITY_INSERT [dbo].[NguoiDanhGia] ON 

INSERT [dbo].[NguoiDanhGia] ([MaNDG], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (1, N'Nguyễn Văn A', N'08/08/1988', 97600453, 1234567890, N'TDM, BD')
INSERT [dbo].[NguoiDanhGia] ([MaNDG], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (29, N'Trà My', N'23/06/2000', 976984828, 12356, N'ĐN')
INSERT [dbo].[NguoiDanhGia] ([MaNDG], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (30, N'Huy', N'03/12/1998', 976984828, 123457891, N'TDM,BD')
INSERT [dbo].[NguoiDanhGia] ([MaNDG], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (31, N'Huy', N'23/06/2000', 976984828, 12356, N'TDM')
INSERT [dbo].[NguoiDanhGia] ([MaNDG], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (32, N'sfghh', N'ghj', 1234, 4531, N'jkl')
INSERT [dbo].[NguoiDanhGia] ([MaNDG], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (33, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[NguoiDanhGia] ([MaNDG], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (34, N'Tường Vi', N'03/12/1998', 7894565, 12356, N'TDM,BD')
INSERT [dbo].[NguoiDanhGia] ([MaNDG], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (35, N'Trà My', N'3/12/1998', 976984828, 123457891, N'TDM')
INSERT [dbo].[NguoiDanhGia] ([MaNDG], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (36, N'Trà My', N'03/12/1998', 7894565, 123457891, N'TDM')
SET IDENTITY_INSERT [dbo].[NguoiDanhGia] OFF
SET IDENTITY_INSERT [dbo].[NguoiDung] ON 

INSERT [dbo].[NguoiDung] ([MaND], [MaCB], [TenDangNhap], [MatKhau], [PhanQuyen]) VALUES (1, 1, N'mymy', N'123', 1)
INSERT [dbo].[NguoiDung] ([MaND], [MaCB], [TenDangNhap], [MatKhau], [PhanQuyen]) VALUES (2, 2, N'mymy', N'1234', 0)
SET IDENTITY_INSERT [dbo].[NguoiDung] OFF
SET IDENTITY_INSERT [dbo].[NguoiPhanAnh] ON 

INSERT [dbo].[NguoiPhanAnh] ([MaNPA], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (1, N'Nguyễn Thị D', N'01/01/1990', 16977503, 1298745637, N'PG, BD')
INSERT [dbo].[NguoiPhanAnh] ([MaNPA], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (2, N'Trần Văn E', N'28/8/1997', 12499687, 1233789784, N'TDM,BD')
INSERT [dbo].[NguoiPhanAnh] ([MaNPA], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (3, N'Lê Thị Q', N'08/08/1998', 45123789, 1234567890, N'ĐN')
INSERT [dbo].[NguoiPhanAnh] ([MaNPA], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (4, N'Tường Vi', N'03/12/1998', 7894565, 12356, N'TDM')
INSERT [dbo].[NguoiPhanAnh] ([MaNPA], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (5, N'Trà My', N'03/12/1998', 976984828, 12345789, N'TDM,BD')
INSERT [dbo].[NguoiPhanAnh] ([MaNPA], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (6, N'Tường Vi', N'23/06/2000', 7894565, 12356, N'ĐN')
INSERT [dbo].[NguoiPhanAnh] ([MaNPA], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (7, N'Hào', N'08/05/2010', 7894565, 123457891, N'ĐN')
INSERT [dbo].[NguoiPhanAnh] ([MaNPA], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (8, N'Tường Vi', N'03/12/1998', 7894565, 12356, N'TDM,BD')
INSERT [dbo].[NguoiPhanAnh] ([MaNPA], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (9, N'Trà My', N'03/12/1998', 976984828, 123457891, N'TDM,BD')
INSERT [dbo].[NguoiPhanAnh] ([MaNPA], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (10, N'Trà My', N'03/12/1998', 976984828, 123457891, N'TDM,BD')
INSERT [dbo].[NguoiPhanAnh] ([MaNPA], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (11, N'Tường Vi', N'23/06/2000', 1233899, 12356, N'TDM')
INSERT [dbo].[NguoiPhanAnh] ([MaNPA], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (12, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[NguoiPhanAnh] ([MaNPA], [Ten], [NgaySinh], [DT], [MaBienNhan], [ThuongTru]) VALUES (13, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[NguoiPhanAnh] OFF
SET IDENTITY_INSERT [dbo].[NoiDung] ON 

INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (2, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (3, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (4, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (5, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (6, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (7, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (8, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (9, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (10, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (11, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (12, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (13, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (14, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (15, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (16, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (17, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (18, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (19, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (20, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (21, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (22, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (23, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (24, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (25, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (26, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (27, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (28, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (29, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (30, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (31, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (32, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (33, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (34, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (35, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (36, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (37, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (38, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (39, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (40, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (41, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (42, NULL, N'abc', NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (43, NULL, N'fghj', NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (44, NULL, NULL, NULL, NULL)
INSERT [dbo].[NoiDung] ([MaNoiD], [NoiDung], [NoiDungChiTiet], [MaND], [MaDG]) VALUES (45, N'abc', N'abc', 1, 1)
SET IDENTITY_INSERT [dbo].[NoiDung] OFF
SET IDENTITY_INSERT [dbo].[PhanAnh] ON 

INSERT [dbo].[PhanAnh] ([MaPA], [MaNPA], [HinhAnh], [NoiDung], [MaBienNhan]) VALUES (1, 1, N'', N'asdfghjkl; ', 1234666)
SET IDENTITY_INSERT [dbo].[PhanAnh] OFF
SET IDENTITY_INSERT [dbo].[SoNganh] ON 

INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (1, N'Sở Tài chính', 1, N'TaiChinh.jpg')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (2, N'Sở Nông nghiệp và Phát triển Nông thôn', 1, N'NongNghiep.png')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (3, N'Ban Quản lý Khu Công nghiệp Việt Nam - Singapore', 1, N'')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (4, N'Sở Kế hoạch - Ðầu tu', 3, N'Kehoach.png')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (5, N'Ban Quản lý các Khu Công nghiệp tỉnh', 2, N'BanquanlyBD.jpg')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (6, N'Sở Lao động - Thương binh và Xã hội', 2, N'LaoDong.jpg')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (7, N'Sở Xây dựng', 1, N'XayDung.jpg')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (8, N'Sở Công Thương', 1, N'Congthuong.png')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (9, N'Sở Tài nguyên - Môi truờng', 1, N'TaiNguyen.jpg')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (10, N'Sở Giáo dục và Ðào tạo', 1, N'GiaoDuc.png')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (11, N'Sở Khoa học - Công nghệ', 1, N'')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (12, N'Sở Giao thông - Vận tải', 5, N'GiaoThong.jpg')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (13, N'Sở Văn hóa, Thể thao và Du lịch', 1, N'Vanhoa.jpg')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (14, N'Sở Thông tin và Truyền thông', 1, N'Thongtin.jpg')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (15, N'Sở Tư pháp', 2, N'TuPhap.jpg')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (16, N'Sở Y tế', 1, N'Yte.jpg')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (17, N'Chi cục Vệ sinh An toàn Thực Phẩm', 1, N'')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (18, N'Uy ban Nhân dân tỉnh', 1, N'Uybannhandan.png')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (19, N'Sở Nội vụ', 1, N'NoiVu1.jpg')
INSERT [dbo].[SoNganh] ([MaS], [TenS], [SoLuong], [Hinh]) VALUES (20, N'Sở Ngoại vụ', 1, N'NgoaiVu.jpg')
SET IDENTITY_INSERT [dbo].[SoNganh] OFF
ALTER TABLE [dbo].[DanhGia]  WITH CHECK ADD FOREIGN KEY([MaCB])
REFERENCES [dbo].[HoSo] ([MaCB])
GO
ALTER TABLE [dbo].[DanhGia]  WITH CHECK ADD FOREIGN KEY([MaNDG])
REFERENCES [dbo].[NguoiDanhGia] ([MaNDG])
GO
ALTER TABLE [dbo].[HoSo]  WITH CHECK ADD FOREIGN KEY([MaDHL])
REFERENCES [dbo].[DoHaiLong] ([MaDHL])
GO
ALTER TABLE [dbo].[HoSo]  WITH CHECK ADD FOREIGN KEY([MaSo])
REFERENCES [dbo].[SoNganh] ([MaS])
GO
ALTER TABLE [dbo].[NguoiDung]  WITH CHECK ADD FOREIGN KEY([MaCB])
REFERENCES [dbo].[HoSo] ([MaCB])
GO
ALTER TABLE [dbo].[NoiDung]  WITH CHECK ADD FOREIGN KEY([MaDG])
REFERENCES [dbo].[DanhGia] ([MaDG])
GO
ALTER TABLE [dbo].[NoiDung]  WITH CHECK ADD FOREIGN KEY([MaND])
REFERENCES [dbo].[NguoiDung] ([MaND])
GO
ALTER TABLE [dbo].[PhanAnh]  WITH CHECK ADD FOREIGN KEY([MaNPA])
REFERENCES [dbo].[NguoiPhanAnh] ([MaNPA])
GO
USE [master]
GO
ALTER DATABASE [QLTTHCC] SET  READ_WRITE 
GO
